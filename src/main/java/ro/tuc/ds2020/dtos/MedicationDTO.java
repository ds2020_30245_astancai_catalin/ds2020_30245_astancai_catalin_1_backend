package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Medication;

import java.util.UUID;

public class MedicationDTO {
    private UUID id;
    private String name;
    private String listOfSideEffects;
    private int dosage;
    private String intakeTime;

    public MedicationDTO(){

    }

    public MedicationDTO(String name, String listOfSideEffects, int dosage,String intakeTime) {
        this.name = name;
        this.listOfSideEffects = listOfSideEffects;
        this.dosage = dosage;
        this.intakeTime= intakeTime;
    }

    public MedicationDTO(UUID id, String name, String listOfSideEffects, int dosage,String intakeTime) {
        this.id = id;
        this.name = name;
        this.listOfSideEffects = listOfSideEffects;
        this.dosage = dosage;
        this.intakeTime= intakeTime;
    }

    public String getIntakeTime() {
        return intakeTime;
    }

    public void setIntakeTime(String intakeTime) {
        this.intakeTime = intakeTime;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getListOfSideEffects() {
        return listOfSideEffects;
    }

    public void setListOfSideEffects(String listOfSideEffects) {
        this.listOfSideEffects = listOfSideEffects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }
}
