package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.CareGiver;
import ro.tuc.ds2020.entities.Doctor;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;

public class DoctorBuilder {
    private DoctorBuilder() {
    }

    public static DoctorDTO toCareGiverDTO(Doctor careGiver) {
        return new DoctorDTO(careGiver.getId(),careGiver.getName(), careGiver.getBirthDate(),careGiver.getGender(),careGiver.getAddress(),careGiver.getUsername(),careGiver.getPassword());
    }

    public static Doctor toEntity(DoctorDTO careGiverDTO) {
        return new Doctor(careGiverDTO.getName(), careGiverDTO.getBirthDate(),careGiverDTO.getGender(),careGiverDTO.getAddress(),careGiverDTO.getUsername(),careGiverDTO.getPassword());
    }
}
