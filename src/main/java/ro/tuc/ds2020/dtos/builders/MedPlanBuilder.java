package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedPlanDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.entities.MedPlan;
import ro.tuc.ds2020.entities.Medication;

public class MedPlanBuilder {
    private MedPlanBuilder() {
    }
    public static MedPlanDTO toMedicationDTO(MedPlan medPlan) {
        return new MedPlanDTO(medPlan.getId(),medPlan.getName(),medPlan.getPatient(),medPlan.getStartPeriodTreatmen(),medPlan.getEndPeriodTreatmen(),medPlan.getTypeOfAdmin(),medPlan.getMedications());

    }
    public static MedPlan toEntity(MedPlanDTO medPlanDTO) {
        return new MedPlan(medPlanDTO.getName(),medPlanDTO.getPatient(),medPlanDTO.getStartPeriodTreatmen(),medPlanDTO.getEndPeriodTreatmen(),medPlanDTO.getTypeOfAdmin(),medPlanDTO.getMedications());
    }
}
