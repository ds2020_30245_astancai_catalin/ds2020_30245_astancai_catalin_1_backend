package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CareGiverDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.CareGiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;

public class CareGiverBuilder {
    private CareGiverBuilder() {
    }

    public static CareGiverDTO toCareGiverDTO(CareGiver careGiver) {
        return new CareGiverDTO(careGiver.getId(),careGiver.getName(), careGiver.getBirthDate(),careGiver.getGender(),careGiver.getAddress(),careGiver.getUsername(),careGiver.getPassword());
    }

    public static CareGiver toEntity(CareGiverDTO careGiverDTO) {
        return new CareGiver(careGiverDTO.getName(), careGiverDTO.getBirthDate(),careGiverDTO.getGender(),careGiverDTO.getAddress(),careGiverDTO.getUsername(),careGiverDTO.getPassword());
    }
}
