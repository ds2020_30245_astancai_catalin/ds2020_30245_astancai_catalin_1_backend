package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.entities.Activity;

public class ActivityBuilder {
    private ActivityBuilder() {
    }

    public static ActivityDTO toActivityDTO(Activity activity) {
        return new ActivityDTO(activity.getId(),activity.getActivity(),activity.getStartDate(),activity.getEndDate(),activity.getPatient());
    }

    public static Activity toEntity(ActivityDTO activityDTO) {
        return new Activity(activityDTO.getActivity(),activityDTO.getStartDate(),activityDTO.getEndDate(),activityDTO.getPatient());

    }
}
