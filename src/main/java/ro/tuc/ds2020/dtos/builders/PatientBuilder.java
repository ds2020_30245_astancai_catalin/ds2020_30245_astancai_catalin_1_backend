package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;

public class PatientBuilder {
    private PatientBuilder() {
    }

    public static PatientDTO toPatientDTO(Patient patient) {
        return new PatientDTO(patient.getId(),patient.getName(), patient.getBirthDate(),patient.getGender(),patient.getAddress(),patient.getMedicalRecord(),patient.getCareGiver(),patient.getUsername(),patient.getPassword());
    }

    public static Patient toEntity(PatientDTO patientDTO) {
        return new Patient(patientDTO.getName(), patientDTO.getBirthDate(),patientDTO.getGender(),patientDTO.getAddress(),patientDTO.getMedicalRecord(),patientDTO.getCareGiver(),patientDTO.getUsername(),patientDTO.getPassword());

    }
}
