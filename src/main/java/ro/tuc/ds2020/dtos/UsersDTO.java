package ro.tuc.ds2020.dtos;
import ro.tuc.ds2020.entities.Patient;

import java.sql.Date;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class UsersDTO {
    private  UUID id;
    private String username;
    private String password;

    public UsersDTO(){

    }
    public UsersDTO(String username, String password) {
        this.username = username;
        this.password = password;
    }
    public UsersDTO(UUID id,String username, String password) {
        this.id=id;
        this.username = username;
        this.password = password;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
