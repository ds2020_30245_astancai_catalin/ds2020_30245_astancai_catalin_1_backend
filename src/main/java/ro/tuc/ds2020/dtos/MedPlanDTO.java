package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Patient;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class MedPlanDTO {

    private UUID id;
    private String name;
    private Patient patient;
    private Date startPeriodTreatmen;
    private Date endPeriodTreatmen;
    private String typeOfAdmin;
    private Set<Medication> medications = new HashSet<>();

    public MedPlanDTO(String name, Patient patient, Date startPeriodTreatmen, Date endPeriodTreatmen, String typeOfAdmin, Set<Medication> medications) {
        this.name = name;
        this.patient = patient;
        this.startPeriodTreatmen = startPeriodTreatmen;
        this.endPeriodTreatmen = endPeriodTreatmen;
        this.typeOfAdmin = typeOfAdmin;
        this.medications = medications;
    }



    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

    public MedPlanDTO(){

    }

    public MedPlanDTO(UUID id, String name, Patient patient, Date startPeriodTreatmen, Date endPeriodTreatmen, String typeOfAdmin, Set<Medication> medications) {
        this.id = id;
        this.name = name;
        this.patient = patient;
        this.startPeriodTreatmen = startPeriodTreatmen;
        this.endPeriodTreatmen = endPeriodTreatmen;
        this.typeOfAdmin = typeOfAdmin;
        this.medications = medications;
    }


    public MedPlanDTO(String name, Patient patient, Date startPeriodTreatmen, Date endPeriodTreatmen, String typeOfAdmin) {
        this.name = name;
        this.patient = patient;
        this.startPeriodTreatmen = startPeriodTreatmen;
        this.endPeriodTreatmen = endPeriodTreatmen;
        this.typeOfAdmin = typeOfAdmin;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getStartPeriodTreatmen() {
        return startPeriodTreatmen;
    }

    public void setStartPeriodTreatmen(Date startPeriodTreatmen) {
        this.startPeriodTreatmen = startPeriodTreatmen;
    }

    public Date getEndPeriodTreatmen() {
        return endPeriodTreatmen;
    }

    public void setEndPeriodTreatmen(Date endPeriodTreatmen) {
        this.endPeriodTreatmen = endPeriodTreatmen;
    }

    public String getTypeOfAdmin() {
        return typeOfAdmin;
    }

    public void setTypeOfAdmin(String typeOfAdmin) {
        this.typeOfAdmin = typeOfAdmin;
    }

    @Override
    public String toString() {
        return medications.toString();
    }

    public String exportDate(){
        return startPeriodTreatmen+" "+endPeriodTreatmen;
    }
}
