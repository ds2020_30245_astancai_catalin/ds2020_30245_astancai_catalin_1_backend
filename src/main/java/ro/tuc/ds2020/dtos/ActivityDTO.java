package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Patient;

import java.sql.Date;
import java.util.UUID;

public class ActivityDTO {

    private UUID id;
    private String activity;
    private Long startDate;
    private Long endDate;
    private Patient patient;

    public ActivityDTO(){

    }
    public ActivityDTO(UUID id, String activity, Long startDate, Long endDate, Patient patient) {
        this.id = id;
        this.activity = activity;
        this.startDate = startDate;
        this.endDate = endDate;
        this.patient = patient;
    }
    public ActivityDTO( String activity, Long startDate, Long endDate, Patient patient) {

        this.activity = activity;
        this.startDate = startDate;
        this.endDate = endDate;
        this.patient = patient;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
