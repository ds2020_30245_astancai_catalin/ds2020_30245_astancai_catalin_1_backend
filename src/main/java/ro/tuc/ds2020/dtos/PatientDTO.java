package ro.tuc.ds2020.dtos;
import org.apache.catalina.User;
import ro.tuc.ds2020.entities.CareGiver;

import javax.persistence.Entity;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.UUID;
import java.util.Objects;
import java.util.UUID;

public class PatientDTO extends UsersDTO {
    private UUID id;
    private String name;
    private Date birthDate;
    private String gender;
    private String address;
    private String medicalRecord;
    private CareGiver careGiver;

    public PatientDTO(){

    }

    public PatientDTO( String name, Date birthDate, String gender, String address, String medicalRecord,CareGiver careGiver,String username, String password) {
        super(username,password);
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.careGiver=careGiver;

    }

    public PatientDTO(UUID id, String name, Date birthDate, String gender, String address, String medicalRecord, CareGiver careGiver,String username, String password) {
        super(username,password);
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.careGiver = careGiver;

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public CareGiver getCareGiver() {
        return careGiver;
    }

    public void setCareGiver(CareGiver careGiver) {
        this.careGiver = careGiver;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

}
