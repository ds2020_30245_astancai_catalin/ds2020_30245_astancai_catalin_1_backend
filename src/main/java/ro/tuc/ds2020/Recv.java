package ro.tuc.ds2020;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.ActivityBuilder;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Activity;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.ActivityRepository;
import ro.tuc.ds2020.services.ActivityService;
import ro.tuc.ds2020.services.PatientService;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeoutException;
@Component
public class Recv {

    private final static String QUEUE_NAME = "PRODUCER";
    private final PatientService patientService;
    private final ActivityService activityService;
    ConnectionFactory factory = new ConnectionFactory();

    @Autowired
    public Recv(PatientService patientService, ActivityService activityService) {
        this.patientService = patientService;
        this.activityService = activityService;
    }

    @RabbitListener(queues = QUEUE_NAME)
    public void connect(String message) {
        System.out.println(" " + message);

        List<ActivityDTO> activityDTOS = new ArrayList<ActivityDTO>();
        List<JSONObject> jsonList = new ArrayList<JSONObject>();
        String back;
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
        JSONObject jsonObject = new JSONObject(message);
        String activityName = jsonObject.getString("activityName");
        float hourSpent = (float) (((jsonObject.getLong("endDate") - jsonObject.getLong("startDate")) / (1000 * 60 * 60)) % 24);
        int minutesSpent = (int) (((jsonObject.getLong("endDate") - jsonObject.getLong("startDate")) / (1000 * 60)) % 60);

        if (activityName.contains("Sleeping") && hourSpent > 7 || (activityName.contains("Leaving") && hourSpent > 5) || activityName.contains("Toileting") && minutesSpent > 30 || activityName.contains("Grooming") && minutesSpent > 30 || activityName.contains("Showering") && minutesSpent > 30) {
                PatientDTO patientDTO = patientService.findPatientById(UUID.fromString(jsonObject.getString("id")));
                Patient patient = PatientBuilder.toEntity(patientDTO);
                patient.setId(UUID.fromString(jsonObject.getString("id")));
                ActivityDTO activityDTO = new ActivityDTO(jsonObject.getString("activityName"), jsonObject.getLong("startDate"), jsonObject.getLong("endDate"), patient);
                activityService.insert(activityDTO);
        }
    }
}