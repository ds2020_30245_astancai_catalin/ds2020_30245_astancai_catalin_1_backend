package ro.tuc.ds2020;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.validation.annotation.Validated;

import java.io.IOException;
import java.util.TimeZone;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
@Validated
public class Ds2020Application {
	public static void main(String[] args) {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		SpringApplication.run(Ds2020Application.class, args);
	}
}
