package ro.tuc.ds2020.RemoteServer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Server {

    private ImplementService implementService;

    @Autowired
    public Server(ImplementService implementService) {
        this.implementService = implementService;
    }


    @Bean(name = "/medicationPlan")
    HttpInvokerServiceExporter accountService() {
        HttpInvokerServiceExporter exporter = new HttpInvokerServiceExporter();
        exporter.setService(implementService);
        exporter.setServiceInterface(MedPlanService.class);
        return exporter;
    }


}