package ro.tuc.ds2020.RemoteServer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.MedPlanDTO;
import ro.tuc.ds2020.dtos.builders.MedPlanBuilder;
import ro.tuc.ds2020.entities.MedPlan;
import ro.tuc.ds2020.repositories.MedPlanRepository;

import java.util.Optional;
import java.util.UUID;
@Service
public class ImplementService implements  MedPlanService {

    private final MedPlanRepository medPlanRepository;
    @Autowired
    public ImplementService(MedPlanRepository medPlanRepository) {
        this.medPlanRepository = medPlanRepository;
    }


    public String sendPlanToClient(UUID patientId) {
        Optional<MedPlan> medPlan = medPlanRepository.findByPatient_Id(patientId);
        MedPlanDTO medplanDto= MedPlanBuilder.toMedicationDTO(medPlan.get());

        String ok = String.valueOf(medplanDto);
        return  ok;
    }
}
