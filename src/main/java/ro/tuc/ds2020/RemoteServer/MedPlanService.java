package ro.tuc.ds2020.RemoteServer;

import java.util.UUID;

public interface MedPlanService {
    String sendPlanToClient (UUID patientId);

}
