package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedPlanDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.builders.MedPlanBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.entities.MedPlan;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.MedPlanRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
@Service
public class MedPlanService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ro.tuc.ds2020.services.MedPlanService.class);

    private final MedPlanRepository medPlanRepository;

    @Autowired
    public MedPlanService(MedPlanRepository medPlanRepository) {
        this.medPlanRepository = medPlanRepository;
    }

    public List<MedPlanDTO> findMedication() {
        List<MedPlan> medicationList = medPlanRepository.findAll();
        return medicationList.stream()
                .map(MedPlanBuilder::toMedicationDTO)
                .collect(Collectors.toList());
    }

    public UUID insert(MedPlanDTO medicationDTO) {
        MedPlan medication = MedPlanBuilder.toEntity(medicationDTO);
        medication = medPlanRepository.save(medication);
        LOGGER.debug("Medplan with id {} was inserted in db", medication.getId());
        return medication.getId();
    }
    public MedPlanDTO findMedicationById(UUID id) {
        Optional<MedPlan> prosumerOptional = medPlanRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("MedpLan with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return MedPlanBuilder.toMedicationDTO(prosumerOptional.get());
    }
    public UUID delete(UUID id){
        Optional<MedPlan> optionalCaregiver = medPlanRepository.findById(id);
        MedPlan caregiver = optionalCaregiver.get();
        medPlanRepository.deleteById(id);
        return id;
    }

}
