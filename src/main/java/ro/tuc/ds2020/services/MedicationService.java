package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CareGiverDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.builders.CareGiverBuilder;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.entities.CareGiver;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.CareGiverRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
@Service
public class MedicationService {
        private static final Logger LOGGER = LoggerFactory.getLogger(ro.tuc.ds2020.services.MedicationService.class);

        private final MedicationRepository medicationRepository;

        @Autowired
        public MedicationService(MedicationRepository medicationRepository) {
            this.medicationRepository = medicationRepository;
        }

        public List<MedicationDTO> findMedication() {
           List<Medication> medicationList = medicationRepository.findAll();
            return medicationList.stream()
                    .map(MedicationBuilder::toMedicationDTO)
                    .collect(Collectors.toList());
        }

        public UUID insert(MedicationDTO medicationDTO) {
            Medication medication = MedicationBuilder.toEntity(medicationDTO);
            medication = medicationRepository.save(medication);
            LOGGER.debug("Medication with id {} was inserted in db", medication.getId());
            return medication.getId();
        }
        public MedicationDTO findMedicationById(UUID id) {
            Optional<Medication> prosumerOptional = medicationRepository.findById(id);
            if (!prosumerOptional.isPresent()) {
                LOGGER.error("CareGiver with id {} was not found in db", id);
                throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
            }
            return MedicationBuilder.toMedicationDTO(prosumerOptional.get());
        }

    public UUID delete(UUID id){
        Optional<Medication> optionalCaregiver = medicationRepository.findById(id);
        Medication caregiver = optionalCaregiver.get();
        medicationRepository.deleteById(id);
        return id;
    }
    public UUID update(UUID id, MedicationDTO medicationDTO){
        Optional<Medication> optionalCaregiver = medicationRepository.findById(id);
        Medication caregiver = optionalCaregiver.get();

        Medication aux = optionalCaregiver.get();

        String name,listofSideEffects;
        int dosage;
        name=medicationDTO.getName();
        listofSideEffects=medicationDTO.getListOfSideEffects();
        dosage=medicationDTO.getDosage();
        aux.setName(name);
        aux.setListOfSideEffects(listofSideEffects);
       aux.setDosage(dosage);

        medicationRepository.save(aux);

        return id;
    }


    }

