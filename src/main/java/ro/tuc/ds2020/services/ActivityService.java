package ro.tuc.ds2020.services;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.dtos.builders.ActivityBuilder;
import ro.tuc.ds2020.entities.Activity;
import ro.tuc.ds2020.repositories.ActivityRepository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ActivityService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ActivityService.class);
    private final ActivityRepository activityRepository;

    @Autowired
    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public List<ActivityDTO> findActivity() {
        List<Activity> activityList = activityRepository.findAll();
        return activityList.stream()
                .map(ActivityBuilder::toActivityDTO)
                .collect(Collectors.toList());
    }


    public UUID insert(ActivityDTO activityDTO) {
        Activity activity = ActivityBuilder.toEntity(activityDTO);
        activity = activityRepository.save(activity);
        LOGGER.debug("Activity with id {} was inserted in db", activity.getId());
        return activity.getId();
    }


}
