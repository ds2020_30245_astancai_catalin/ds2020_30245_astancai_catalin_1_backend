package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PatientDTO;

import ro.tuc.ds2020.dtos.builders.PatientBuilder;

import ro.tuc.ds2020.entities.CareGiver;
import ro.tuc.ds2020.entities.Patient;

import ro.tuc.ds2020.repositories.PatientRepository;


import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
@Service
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<PatientDTO> findPatient() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public UUID insert(PatientDTO patientDTO) {
        Patient patient = PatientBuilder.toEntity(patientDTO);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public PatientDTO findPatientById(UUID id) {
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDTO(prosumerOptional.get());
    }

    public UUID delete(UUID id){
        Optional<Patient> optionalCaregiver = patientRepository.findById(id);
        Patient caregiver = optionalCaregiver.get();
        patientRepository.deleteById(id);
        return id;
    }

    public UUID update(UUID id, PatientDTO patientDTO){
        Optional<Patient> optionalCaregiver = patientRepository.findById(id);
        Patient patient = optionalCaregiver.get();

        String name = patientDTO.getName();
        Date bithDate= patientDTO.getBirthDate();
        String gender = patientDTO.getGender();
        String address = patientDTO.getAddress();
        String medicalRecord = patientDTO.getMedicalRecord();
        String username=patientDTO.getUsername();
        String password=patientDTO.getPassword();

        UUID x  =patientDTO.getCareGiver().getId();
        CareGiver aux = patientDTO.getCareGiver();


        aux.setId(x);
        patient.setName(name);
        patient.setBirthDate(bithDate);
        patient.setGender(gender);
        patient.setAddress(address);
        patient.setMedicalRecord(medicalRecord);
        patient.setCareGiver(aux);
        patient.setUsername(username);
        patient.setPassword(password);

        patientRepository.save(patient);
        return id;





    }

}
