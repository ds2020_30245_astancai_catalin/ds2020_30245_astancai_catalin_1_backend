package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CareGiverDTO;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.builders.CareGiverBuilder;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.CareGiver;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.CareGiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CareGiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CareGiverService.class);
    private final CareGiverRepository careGiverRepository;
    private final PatientRepository patientRepository;

    @Autowired
    public CareGiverService(CareGiverRepository careGiverRepository,PatientRepository patientRepository) {
        this.careGiverRepository = careGiverRepository;
        this.patientRepository=patientRepository;
    }

    public List<CareGiverDTO> findCareGiver() {
        List<CareGiver> careGiverList = careGiverRepository.findAll();
        return careGiverList.stream()
                .map(CareGiverBuilder::toCareGiverDTO)
                .collect(Collectors.toList());
    }

    public UUID insert(CareGiverDTO careGiverDTO) {
        CareGiver careGiver = CareGiverBuilder.toEntity(careGiverDTO);
        careGiver = careGiverRepository.save(careGiver);
        LOGGER.debug("CareGiver with id {} was inserted in db", careGiver.getId());
        return careGiver.getId();
    }
    public CareGiverDTO findCareGiverById(UUID id) {
        Optional<CareGiver> prosumerOptional = careGiverRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("CareGiver with id {} was not found in db", id);
           // throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return CareGiverBuilder.toCareGiverDTO(prosumerOptional.get());
    }

    public UUID delete(UUID id){
        Optional<CareGiver> optionalCaregiver = careGiverRepository.findById(id);
        CareGiver caregiver = optionalCaregiver.get();
        for(Patient pat : caregiver.getListOfPatients()){
            pat.setCareGiver(null);
            patientRepository.save(pat);
        }

        careGiverRepository.deleteById(id);
        return id;
    }
    public UUID update(UUID id, CareGiverDTO careGiverDTO){
        Optional<CareGiver> optionalCaregiver = careGiverRepository.findById(id);
        CareGiver caregiver = optionalCaregiver.get();

        CareGiver aux = optionalCaregiver.get();

        String name,gender,address;
        Date birthDate;
        name=careGiverDTO.getName();
        gender=careGiverDTO.getGender();
        address=careGiverDTO.getAddress();
        birthDate=careGiverDTO.getBirthDate();
        String username  = careGiverDTO.getUsername();
        String password  = careGiverDTO.getPassword();

        aux.setName(name);
        aux.setGender(gender);
        aux.setAddress(address);
        aux.setBirthDate(birthDate);
        aux.setUsername(username);
        aux.setPassword(password);

        careGiverRepository.save(aux);

        return id;
    }

}
