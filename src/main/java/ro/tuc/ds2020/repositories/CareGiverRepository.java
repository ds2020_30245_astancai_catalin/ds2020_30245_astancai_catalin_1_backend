package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.CareGiver;
import ro.tuc.ds2020.entities.Patient;

import java.util.List;
import java.util.UUID;

public interface CareGiverRepository extends JpaRepository<CareGiver, UUID> {

    List<CareGiver> findByName(String name);
}