package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.dtos.MedPlanDTO;
import ro.tuc.ds2020.dtos.builders.MedPlanBuilder;
import ro.tuc.ds2020.entities.MedPlan;
import ro.tuc.ds2020.entities.Medication;

import javax.swing.text.html.Option;
import java.util.Optional;
import java.util.UUID;

public interface MedPlanRepository extends JpaRepository<MedPlan, UUID> {
    Optional<MedPlan> findByPatient_Id(UUID patient);

}
