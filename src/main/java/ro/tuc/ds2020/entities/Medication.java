package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;
@Entity
public class Medication implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "listOfSideEffects", nullable = false)
    private String listOfSideEffects;

    @Column(name = "dosage", nullable = false)
    private int dosage;

    @Column(name = "intakeTime", nullable = false)
    private String intakeTime;

    public Medication(String name, String listOfSideEffects, int dosage,String intakeTime) {
        this.name = name;
        this.listOfSideEffects = listOfSideEffects;
        this.dosage = dosage;
        this.intakeTime = intakeTime;
    }
    public Medication(){

    }

    public String getIntakeTime() {
        return intakeTime;
    }

    public void setIntakeTime(String intakeTime) {
        this.intakeTime = intakeTime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getListOfSideEffects() {
        return listOfSideEffects;
    }

    public void setListOfSideEffects(String listOfSideEffects) {
        this.listOfSideEffects = listOfSideEffects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    @Override
    public String toString() {
        return
              name+" "+
              intakeTime
                ;
    }
}
