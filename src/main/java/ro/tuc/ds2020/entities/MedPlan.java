package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
@Entity
public class MedPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient", nullable = false)
    private Patient patient;

    @Column(name = "startPeriodTreatment", nullable = false)
    private Date startPeriodTreatmen;

    @Column(name = "endPeriodTreatment", nullable = false)
    private Date endPeriodTreatmen;

    @Column(name = "typeOfAdmin", nullable = false)
    private String typeOfAdmin;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "medToMedPlan",
            joinColumns = { @JoinColumn(name = "medicationId") },
            inverseJoinColumns = { @JoinColumn(name = "medPlanId") }
    )
    private Set<Medication> medications = new HashSet<>();

    public Set<Medication> getMedications() {
        return medications;
    }

    public void setMedications(Set<Medication> medications) {
        this.medications = medications;
    }

    public MedPlan(String name, Patient patient, Date startPeriodTreatmen, Date endPeriodTreatmen, String typeOfAdmin, Set<Medication> medications) {
        this.name = name;
        this.patient = patient;
        this.startPeriodTreatmen = startPeriodTreatmen;
        this.endPeriodTreatmen = endPeriodTreatmen;
        this.typeOfAdmin = typeOfAdmin;
        this.medications = medications;
    }

    public MedPlan(String name, Patient patient, Date startPeriodTreatmen, Date endPeriodTreatmen, String typeOfAdmin) {
        this.name = name;
        this.patient = patient;
        this.startPeriodTreatmen = startPeriodTreatmen;
        this.endPeriodTreatmen = endPeriodTreatmen;
        this.typeOfAdmin = typeOfAdmin;
    }
    public MedPlan(){}

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Date getStartPeriodTreatmen() {
        return startPeriodTreatmen;
    }

    public void setStartPeriodTreatmen(Date startPeriodTreatmen) {
        this.startPeriodTreatmen = startPeriodTreatmen;
    }

    public Date getEndPeriodTreatmen() {
        return endPeriodTreatmen;
    }

    public void setEndPeriodTreatmen(Date endPeriodTreatmen) {
        this.endPeriodTreatmen = endPeriodTreatmen;
    }

    public String getTypeOfAdmin() {
        return typeOfAdmin;
    }

    public void setTypeOfAdmin(String typeOfAdmin) {
        this.typeOfAdmin = typeOfAdmin;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", patient=" + patient +
                ", medications=" + medications +
                '}';
    }
}
