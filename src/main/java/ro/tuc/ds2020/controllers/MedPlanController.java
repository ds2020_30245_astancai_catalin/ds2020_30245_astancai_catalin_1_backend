package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedPlanDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.services.MedPlanService;
import ro.tuc.ds2020.services.MedicationService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medPlan")
public class MedPlanController {

    private final MedPlanService medicationService;

    @Autowired
    public MedPlanController (MedPlanService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping()
    public ResponseEntity<List<MedPlanDTO>> getMedication() {
        List<MedPlanDTO> dtos =  medicationService.findMedication();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertMedication(@RequestBody MedPlanDTO medicationDTO) {
        UUID medicationID = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedPlanDTO> getCareGiverID(@PathVariable("id") UUID careGiverId) {
        MedPlanDTO dto = medicationService.findMedicationById(careGiverId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<UUID>deleteMedication(@PathVariable("id") UUID id)
    {
        UUID careGiverId= medicationService.delete(id);

        return new ResponseEntity<>(careGiverId,HttpStatus.OK);
    }
}
