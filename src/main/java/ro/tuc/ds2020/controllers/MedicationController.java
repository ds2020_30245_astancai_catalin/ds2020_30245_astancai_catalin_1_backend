package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CareGiverDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;

import ro.tuc.ds2020.services.CareGiverService;
import ro.tuc.ds2020.services.MedicationService;

import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {
    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping()
    public ResponseEntity<List<MedicationDTO>> getMedication() {
        List<MedicationDTO> dtos =  medicationService.findMedication();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertMedication( @RequestBody MedicationDTO medicationDTO) {
        UUID medicationID = medicationService.insert(medicationDTO);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<MedicationDTO> getCareGiverID(@PathVariable("id") UUID careGiverId) {
        MedicationDTO dto = medicationService.findMedicationById(careGiverId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<UUID>deleteMedication(@PathVariable("id") UUID id)
    {
        UUID careGiverId= medicationService.delete(id);

        return new ResponseEntity<>(careGiverId,HttpStatus.OK);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<UUID>updateMedication(@RequestBody MedicationDTO medicationDTO)
    {
        UUID careGiverId= medicationService.update(medicationDTO.getId(),medicationDTO);

        return new ResponseEntity<>(careGiverId,HttpStatus.OK);
    }


}
