package ro.tuc.ds2020.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.PatientService;
import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {
    private final PatientService patientService;
    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping()
    public ResponseEntity<List<PatientDTO>> getPatient() {
        List<PatientDTO> dtos =  patientService.findPatient();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
    @PostMapping()
    public ResponseEntity<UUID> insertPatient( @RequestBody PatientDTO patientDTO) {
        UUID patientID = patientService.insert(patientDTO);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
    }
    @GetMapping(value = "/{id}")
    public ResponseEntity<PatientDTO> getPerson(@PathVariable("id") UUID personId) {
        PatientDTO dto = patientService.findPatientById(personId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<UUID>deletePatient(@PathVariable("id") UUID id)
    {
        UUID careGiverId= patientService.delete(id);

        return new ResponseEntity<>(careGiverId,HttpStatus.OK);
    }
    @PutMapping(value = "/update")
    public ResponseEntity<UUID>updateCareGiver(@RequestBody PatientDTO patientDTO)
    {
        UUID careGiverId= patientService.update(patientDTO.getId(),patientDTO);

        return new ResponseEntity<>(careGiverId,HttpStatus.OK);
    }


}
