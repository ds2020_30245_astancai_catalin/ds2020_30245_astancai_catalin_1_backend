package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CareGiverDTO;

import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.services.CareGiverService;
import ro.tuc.ds2020.services.DoctorService;

import javax.persistence.PostUpdate;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final DoctorService careGiverService;

    @Autowired
    public DoctorController(DoctorService careGiverService) {
        this.careGiverService = careGiverService;
    }

    @GetMapping()
    public ResponseEntity<List<DoctorDTO>> getCareGiver() {
        List<DoctorDTO> dtos =  careGiverService.findCareGiver();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertCareGiver( @RequestBody DoctorDTO careGiverDTO) {
        UUID careGiverID = careGiverService.insert(careGiverDTO);
        return new ResponseEntity<>(careGiverID, HttpStatus.CREATED);
    }
}
