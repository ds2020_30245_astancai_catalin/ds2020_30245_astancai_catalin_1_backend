package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CareGiverDTO;

import ro.tuc.ds2020.services.CareGiverService;

import javax.persistence.PostUpdate;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CareGiverController {
    private final CareGiverService careGiverService;

    @Autowired
    public CareGiverController(CareGiverService careGiverService) {
        this.careGiverService = careGiverService;
    }

    @GetMapping()
    public ResponseEntity<List<CareGiverDTO>> getCareGiver() {
        List<CareGiverDTO> dtos =  careGiverService.findCareGiver();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertCareGiver( @RequestBody CareGiverDTO careGiverDTO) {
        UUID careGiverID = careGiverService.insert(careGiverDTO);
        return new ResponseEntity<>(careGiverID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CareGiverDTO> getCareGiverID(@PathVariable("id") UUID careGiverId) {
        CareGiverDTO dto = careGiverService.findCareGiverById(careGiverId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<UUID>deleteCareGiver(@PathVariable("id") UUID id)
    {
        UUID careGiverId= careGiverService.delete(id);

        return new ResponseEntity<>(careGiverId,HttpStatus.OK);
    }

    @PutMapping(value = "/update")
    public ResponseEntity<UUID>updateCareGiver(@RequestBody CareGiverDTO careGiverDTO)
    {
        UUID careGiverId= careGiverService.update(careGiverDTO.getId(),careGiverDTO);

        return new ResponseEntity<>(careGiverId,HttpStatus.OK);
    }

}
