package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.tuc.ds2020.dtos.ActivityDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.services.ActivityService;
import ro.tuc.ds2020.services.PatientService;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/activity")
public class ActivityController {

    private final ActivityService activityService;
    @Autowired
    public ActivityController(ActivityService activityService) {
        this.activityService = activityService;
    }

    @GetMapping()
    public ResponseEntity<List<ActivityDTO>> getActivity() {
        List<ActivityDTO> dtos =  activityService.findActivity();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
}
